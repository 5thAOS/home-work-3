package com.example.rany.fragmenthomework;

import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.rany.fragmenthomework.model.Article;

public class ResultActivity extends AppCompatActivity {

    String img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Article article = (Article) getIntent().getParcelableArrayListExtra("object");
        img = article.getImage();
        addFragment();
    }

    public  void addFragment(){
        ResultFragment fragment = new ResultFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(fragment, "fragment");
        fragment.setSetImageUrl(Uri.parse(img));
        transaction.commit();
    }
}
