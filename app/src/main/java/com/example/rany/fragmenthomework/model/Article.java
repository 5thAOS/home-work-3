package com.example.rany.fragmenthomework.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.stream.Stream;

public class Article extends ArrayList<Parcelable> implements Parcelable {

    private String title;
    private String desc;
    private String image;
    private String date;

    public Article(String title, String desc, String image, String date) {
        this.title = title;
        this.desc = desc;
        this.image = image;
        this.date = date;
    }

    protected Article(Parcel in) {
        title = in.readString();
        desc = in.readString();
        image = in.readString();
        date = in.readString();
    }

    public static final Creator<Article> CREATOR = new Creator<Article>() {
        @Override
        public Article createFromParcel(Parcel in) {
            return new Article(in);
        }

        @Override
        public Article[] newArray(int size) {
            return new Article[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(desc);
        dest.writeString(image);
        dest.writeString(date);
    }

    @Override
    public Stream<Parcelable> stream() {
        return null;
    }
}
