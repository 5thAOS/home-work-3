package com.example.rany.fragmenthomework.adapter;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rany.fragmenthomework.R;
import com.example.rany.fragmenthomework.listener.OnItemClickListener;
import com.example.rany.fragmenthomework.model.Article;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyAdapter extends BaseAdapter {

    List<Article> articleList;
    OnItemClickListener listener;

    public MyAdapter(List<Article> articleList) {
        this.articleList = articleList;
    }

    public void setListener(OnItemClickListener listener){
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return articleList.size();
    }

    @Override
    public Object getItem(int position) {
        return articleList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ImageView imgArticle, icon;
        TextView tvTitle, tvDes;

        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.content_layout, parent, false);

        imgArticle = view.findViewById(R.id.imgArticle);
        icon = view.findViewById(R.id.icDetail);
        tvTitle = view.findViewById(R.id.tvTitle);
        tvDes = view.findViewById(R.id.tvDesc);

        Article article = articleList.get(position);
        tvTitle.setText(article.getTitle());
        tvDes.setText(article.getDesc());
        Picasso.get()
                .load("https://pbs.twimg.com/profile_images/758208606240137216/F_0DfGBU_400x400.jpg")
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_foreground)
                .resize(100, 100)
                .into(imgArticle);

        icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });

        return view;
    }
}
