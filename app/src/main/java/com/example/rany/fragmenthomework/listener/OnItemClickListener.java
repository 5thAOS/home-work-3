package com.example.rany.fragmenthomework.listener;

public interface OnItemClickListener {

    void onItemClick(int position);

}
