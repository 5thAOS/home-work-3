package com.example.rany.fragmenthomework;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.rany.fragmenthomework.adapter.MyAdapter;
import com.example.rany.fragmenthomework.listener.OnItemClickListener;
import com.example.rany.fragmenthomework.model.Article;
import com.example.rany.fragmenthomework.resource.ArticleResource;

public class MainActivity extends AppCompatActivity implements OnItemClickListener{

    private ListView lvContent;
    private MyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvContent = findViewById(R.id.lvContent);
        adapter = new MyAdapter(ArticleResource.getDataSource());
        adapter.setListener(this);
        lvContent.setAdapter(adapter);


    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(this, ResultActivity.class);
        Article article = (Article) adapter.getItem(position);
        intent.putParcelableArrayListExtra("object", article);
        startActivity(intent);

    }
}
