package com.example.rany.fragmenthomework;


import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;


/**
 * A simple {@link Fragment} subclass.
 */
public class ResultFragment extends Fragment {

    private Uri imageUrl;
    public void setSetImageUrl(Uri setImageUrl){
        this.imageUrl = setImageUrl;
    }
    private  ImageView imvResult;

    public ResultFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragmen

        View view =  inflater.inflate(R.layout.fragment_result, container, false);
        imvResult = view.findViewById(R.id.imvResult);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Picasso.get()
                .load(imageUrl)
                .error(R.drawable.ic_launcher_foreground)
                .into(imvResult);
    }
}
