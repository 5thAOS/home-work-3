package com.example.rany.fragmenthomework.resource;

import com.example.rany.fragmenthomework.model.Article;

import java.util.ArrayList;

public class ArticleResource {
    public static ArrayList<Article> getDataSource(){
        ArrayList<Article> articles = new ArrayList<>();
        articles.add(new Article("World Cup 2018",
                "From 18 April (11H CET / 12H Moscow Time)...",
                "https://pbs.twimg.com/profile_images/758208606240137216/F_0DfGBU_400x400.jpg",
                "2018"));
        articles.add(new Article("World Cup 2018",
                "From 18 April (11H CET / 12H Moscow Time)...",
                "https://pbs.twimg.com/profile_images/758208606240137216/F_0DfGBU_400x400.jpg","2018"));
        articles.add(new Article("World Cup 2018",
                "From 18 April (11H CET / 12H Moscow Time)...",
                "https://pbs.twimg.com/profile_images/758208606240137216/F_0DfGBU_400x400.jpg","2018"));
        articles.add(new Article("World Cup 2018",
                "From 18 April (11H CET / 12H Moscow Time)...",
                "https://pbs.twimg.com/profile_images/758208606240137216/F_0DfGBU_400x400.jpg","2018"));
        articles.add(new Article("World Cup 2018",
                "From 18 April (11H CET / 12H Moscow Time)...",
                "https://image.jimcdn.com/app/cms/image/transf/dimension=641x10000:format=png/path/s6d54c624f88c95ec/image/i34d2e10b38f67263/version/1490980233/fifa-world-cup.png",
                "2018"));
        articles.add(new Article("World Cup 2018",
                "From 18 April (11H CET / 12H Moscow Time)...",
                "http://www.abc.net.au/news/image/9816508-3x2-700x467.jpg","2018"));

        return articles;
    }

}
